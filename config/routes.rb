Rails.application.routes.draw do
  resources :daily_attachments
  resources :daily_comments
  # resources :diaries
  # resources :dailies, except: [:index]
  # get '/diaries/:id/dailies', to: 'dailies#index', as: 'diary_dailies'
  
  resources :diaries, except: [:show] do
    resources :dailies
  end

  devise_for :users, :controllers => { omniauth_callbacks: 'user/omniauth_callbacks'}
  
  get 'diary/community', to: 'diaries#show'
  get 'diary/mydiary'
  post 'diaries/:id/like' => 'likes#like_toggle', as: 'like'
  get '/about', to: 'home#about'

  root 'home#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
