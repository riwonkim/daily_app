class Daily < ApplicationRecord
    belongs_to :user, optional: true
    belongs_to :diary, optional: true
    has_many :daily_attachments
    accepts_nested_attributes_for :daily_attachments
    
    has_many  :daily_comments
    
    validates :title, presence: true, length: { minimum: 2, maximum: 100 }
    validates :content, presence: true, length: { minimum: 2 }
  
  

end
