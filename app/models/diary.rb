class Diary < ApplicationRecord
    mount_uploader :profile_img, AvatarUploader
    belongs_to :user
    has_many :dailies
    has_many :likes
    has_many :liking_users, through: :likes, source: :user
    
    validates :title, presence: true, length: { minimum: 2, maximum: 20 }
    validates :content, presence: true, length: { minimum: 2, maximum: 50 }
    validates :budget, presence: true
    validates :start_date, presence: true
    validates :end_date, presence: true
    validates :start_city, presence: true
    validates :end_city, presence: true
    validates :country_count, presence: true
    validates :profile_img, presence: true
    
end
