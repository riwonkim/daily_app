json.extract! daily_comment, :id, :content, :created_at, :updated_at
json.url daily_comment_url(daily_comment, format: :json)
