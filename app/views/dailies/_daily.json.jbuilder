json.extract! daily, :id, :title, :today, :content, :country, :created_at, :updated_at
json.url daily_url(daily, format: :json)
