json.extract! diary, :id, :start_date, :end_date, :title, :content, :budget, :start_city, :end_city, :country_count, :eurail, :created_at, :updated_at
json.url diary_url(diary, format: :json)
