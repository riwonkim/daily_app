json.extract! daily_attachment, :id, :daily_id, :avatar, :created_at, :updated_at
json.url daily_attachment_url(daily_attachment, format: :json)
