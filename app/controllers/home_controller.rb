class HomeController < ApplicationController
  def index
    @diaries = Diary.all
    a = {}
    @diaries.each do |diary|
      a[diary] = diary.likes.count
    end
    a = a.sort_by { |k, v| v }
    result = []
    a.each do |x|
      result << x.first
    end
    
    @popular_diaries = result.last(3).reverse
    # debugger
  end
  
  def about
  end
end
