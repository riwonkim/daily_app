class DailyCommentsController < ApplicationController
  before_action :set_daily_comment, only: [:show, :edit, :update, :destroy]

  # GET /daily_comments
  # GET /daily_comments.json
  def index
    @daily_comments = DailyComment.all
  end

  # GET /daily_comments/1
  # GET /daily_comments/1.json
  def show
    @daily_comment = DailyComment.find(params[:id])
    @daily_comments = DailyComment.all
  end
  
  def new
    @daily_comment = DailyComment.new
  end

  # GET /daily_comments/1/edit
  def edit
    @daily_comment = DailyComment.find(params[:id])
  end

  # POST /daily_comments
  # POST /daily_comments.json
  def create
    @daily_comment = DailyComment.new(daily_comment_params)
    # debugger
    respond_to do |format|
      if @daily_comment.save
        format.html {redirect_back(fallback_location: root_path)}
        format.js {}
        format.json {render 'new_daily_comment'}

      else
        redirect_to new_daily_comment_url(@daily_comment)
      end
    end
  end
  
  # PATCH/PUT /daily_comments/1
  # PATCH/PUT /daily_comments/1.json
  def update
    @daily_comment = DailyComment.find(params[:id])
    
    if @daily_comment.update(daily_comment_params)
      respond_to do |format|
        format.html { redirect_to diary_daily_url(@daily_comment.diary_id, @daily_comment.daily), notice: 'Daily comment was successfully updated.' }
        format.js
      end
    else
        redirect_back(fallback_location: root_path)
    end
  end
  
  # DELETE /daily_comments/1
  # DELETE /daily_comments/1.json
  def destroy
    
    @daily_comment.destroy
    respond_to do |format|
      format.html { redirect_to daily_comments_url, notice: 'Daily comment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_daily_comment
      @daily_comment = DailyComment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def daily_comment_params
      params.require(:daily_comment).permit(:content, :daily_id, :diary_id, :user_id)
    end
    

end