class DailyAttachmentsController < ApplicationController
  before_action :set_daily_attachment, only: [:show, :edit, :update, :destroy]

  # GET /daily_attachments
  # GET /daily_attachments.json
  def index
    @daily_attachments = DailyAttachment.all
  end

  # GET /daily_attachments/1
  # GET /daily_attachments/1.json
  def show
  end

  # GET /daily_attachments/new
  def new
    @daily_attachment = DailyAttachment.new
  end

  # GET /daily_attachments/1/edit
  def edit
  end

  # POST /daily_attachments
  # POST /daily_attachments.json
  def create
    @daily_attachment = DailyAttachment.new(daily_attachment_params)

    respond_to do |format|
      if @daily_attachment.save
        format.html { redirect_to @daily_attachment, notice: 'Daily attachment was successfully created.' }
        format.json { render :show, status: :created, location: @daily_attachment }
      else
        format.html { render :new }
        format.json { render json: @daily_attachment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /daily_attachments/1
  # PATCH/PUT /daily_attachments/1.json
  def update
    respond_to do |format|
      if @daily_attachment.update(daily_attachment_params)
        format.html { redirect_to @daily_attachment.daily, notice: 'Daily attachment was successfully updated.' }
        format.json { render :show, status: :ok, location: @daily_attachment }
      end
    end
  end

  # DELETE /daily_attachments/1
  # DELETE /daily_attachments/1.json
  def destroy
    @daily_attachment.destroy
    respond_to do |format|
      format.html { redirect_to daily_attachments_url, notice: 'Daily attachment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_daily_attachment
      @daily_attachment = DailyAttachment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def daily_attachment_params
      params.require(:daily_attachment).permit(:daily_id, :avatar)
    end
end
