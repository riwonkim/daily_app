class LikesController < ApplicationController
    def like_toggle
        @id = params[:id]
        @like = Like.find_by(user: current_user, diary_id: @id)
        if @like.nil?
            Like.create(user: current_user, diary_id: @id)
        else
            @like.destroy
        end
       respond_to do |format|
          format.js 
       end
    end
end
