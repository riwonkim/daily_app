class DailiesController < ApplicationController
  before_action :set_daily, only: [:indx, :show, :edit, :update, :destroy]
  
  # GET /dailies
  # GET /dailies.json
  # daily 작성한 것 목록 보여주는 화면
  def index
    @diary = Diary.find params[:diary_id]
    @dailies = @diary.dailies
    
 if Like.find_by(user: current_user, diary: @diary).nil?
      @button = '좋아요'
    else
      @button = '좋아요 취소'
    end  end

  # GET /dailies/1
  # GET /dailies/1.json
  def show
    # daily 누르면 보여주는 화면임
    @diary = Diary.find params[:diary_id]
    @daily_comment = DailyComment.new
    @daily_comments = @daily.daily_comments
    @daily_attachments = @daily.daily_attachments.all
    # @daily_attachments = DailyAttachment.find(@daily)
    
  end

  # GET /dailies/new
  # daily 입력 받아오기 !
  def new
    @diary = Diary.find params[:diary_id]
    @daily = Daily.new
    @daily_attachment = @daily.daily_attachments.build
  end

  # GET /dailies/1/edit
  def edit
    # daily 수정입력
  end

  # POST /dailies
  # POST /dailies.json
  # daily 를 저장합니당 
  def create
    #daily_params = params.require(:daily).permit(:title, :today, :content, :country)
    @daily = Daily.new(daily_params)
    # debugger
    if @daily.save
      params[:daily_attachments]['avatar'].each do |a|
          @daily_attachment = @daily.daily_attachments.create!(:avatar => a)
      end
      redirect_to diary_daily_url(@daily.diary, @daily)
    else
      redirect_to new_diary_daily_url(params[:diary_id])
    end
  end

  # PATCH/PUT /dailies/1
  # PATCH/PUT /dailies/1.json
  def update
    # daily 수정해서 저장해
    #daily_params = params.require(:daily).permit(:title, :today, :content, :country)
    
    if @daily.update(daily_params)
      redirect_to daily_url(@daily)
    else 
      redirect_to edit_daily_url(@daily)
    end
  end

  # DELETE /dailies/1
  # DELETE /dailies/1.json
  def destroy
    @daily.destroy
    respond_to do |format|
      format.html { redirect_to diary_dailies_url, notice: 'Daily was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_daily
      @daily = Daily.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def daily_params
      params.require(:daily).permit(:title, :today, :content, :country, :user_id, :diary_id, daily_attachments_attributes: [:id, :daily_id, :avatar])
    end
    
end
