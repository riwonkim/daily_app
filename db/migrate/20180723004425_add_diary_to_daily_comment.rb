class AddDiaryToDailyComment < ActiveRecord::Migration[5.1]
  def change
    add_reference :daily_comments, :diary, foreign_key: true
  end
end
