class CreateDailyAttachments < ActiveRecord::Migration[5.1]
  def change
    create_table :daily_attachments do |t|
      t.integer :daily_id
      t.string :avatar
      
      t.timestamps
    end
  end
end
