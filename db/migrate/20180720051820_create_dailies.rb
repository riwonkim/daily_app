class CreateDailies < ActiveRecord::Migration[5.1]
  def change
    create_table :dailies do |t|
      t.string :title
      t.date :today
      t.string :content
      t.string :country
      
      
      t.belongs_to :user
      t.belongs_to :diary
      
      t.timestamps
    end
  end
end
