class CreateCalens < ActiveRecord::Migration[5.1]
  def change
    create_table :calens do |t|
      t.string :event
      t.datetime :start_time
      t.datetime :end_time

      t.timestamps
    end
  end
end
