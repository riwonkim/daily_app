class CreateDailyComments < ActiveRecord::Migration[5.1]
  def change
    create_table :daily_comments do |t|
      t.string :content
      
      t.belongs_to :user
      t.belongs_to  :daily

      t.timestamps
    end
  end
end