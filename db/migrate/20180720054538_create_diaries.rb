class CreateDiaries < ActiveRecord::Migration[5.1]
  def change
    create_table :diaries do |t|
      t.date :start_date
      t.date :end_date
      t.string :title
      t.string :content
      t.integer :budget
      t.string :start_city
      t.string :end_city
      t.integer :country_count
      t.boolean :eurail
      t.boolean :secret
      t.string :profile_img
      
      t.belongs_to :user

      t.timestamps
    end
  end
end
