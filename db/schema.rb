# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180723065942) do

  create_table "calens", force: :cascade do |t|
    t.string "event"
    t.datetime "start_time"
    t.datetime "end_time"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "dailies", force: :cascade do |t|
    t.string "title"
    t.date "today"
    t.string "content"
    t.string "country"
    t.integer "user_id"
    t.integer "diary_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["diary_id"], name: "index_dailies_on_diary_id"
    t.index ["user_id"], name: "index_dailies_on_user_id"
  end

  create_table "daily_attachments", force: :cascade do |t|
    t.integer "daily_id"
    t.string "avatar"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "daily_comments", force: :cascade do |t|
    t.string "content"
    t.integer "user_id"
    t.integer "daily_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "diary_id"
    t.index ["daily_id"], name: "index_daily_comments_on_daily_id"
    t.index ["diary_id"], name: "index_daily_comments_on_diary_id"
    t.index ["user_id"], name: "index_daily_comments_on_user_id"
  end

  create_table "diaries", force: :cascade do |t|
    t.date "start_date"
    t.date "end_date"
    t.string "title"
    t.string "content"
    t.integer "budget"
    t.string "start_city"
    t.string "end_city"
    t.integer "country_count"
    t.boolean "eurail"
    t.boolean "secret"
    t.string "profile_img"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_diaries_on_user_id"
  end

  create_table "identities", force: :cascade do |t|
    t.integer "user_id"
    t.string "provider"
    t.string "uid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_identities_on_user_id"
  end

  create_table "likes", force: :cascade do |t|
    t.integer "user_id"
    t.integer "diary_id"
    t.index ["diary_id"], name: "index_likes_on_diary_id"
    t.index ["user_id"], name: "index_likes_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "profile_img"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
