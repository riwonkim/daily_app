require 'test_helper'

class DailyCommentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @daily_comment = daily_comments(:one)
  end

  test "should get index" do
    get daily_comments_url
    assert_response :success
  end

  test "should get new" do
    get new_daily_comment_url
    assert_response :success
  end

  test "should create daily_comment" do
    assert_difference('DailyComment.count') do
      post daily_comments_url, params: { daily_comment: { content: @daily_comment.content } }
    end

    assert_redirected_to daily_comment_url(DailyComment.last)
  end

  test "should show daily_comment" do
    get daily_comment_url(@daily_comment)
    assert_response :success
  end

  test "should get edit" do
    get edit_daily_comment_url(@daily_comment)
    assert_response :success
  end

  test "should update daily_comment" do
    patch daily_comment_url(@daily_comment), params: { daily_comment: { content: @daily_comment.content } }
    assert_redirected_to daily_comment_url(@daily_comment)
  end

  test "should destroy daily_comment" do
    assert_difference('DailyComment.count', -1) do
      delete daily_comment_url(@daily_comment)
    end

    assert_redirected_to daily_comments_url
  end
end
