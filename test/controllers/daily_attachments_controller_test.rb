require 'test_helper'

class DailyAttachmentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @daily_attachment = daily_attachments(:one)
  end

  test "should get index" do
    get daily_attachments_url
    assert_response :success
  end

  test "should get new" do
    get new_daily_attachment_url
    assert_response :success
  end

  test "should create daily_attachment" do
    assert_difference('DailyAttachment.count') do
      post daily_attachments_url, params: { daily_attachment: { avatar: @daily_attachment.avatar, daily_id: @daily_attachment.daily_id } }
    end

    assert_redirected_to daily_attachment_url(DailyAttachment.last)
  end

  test "should show daily_attachment" do
    get daily_attachment_url(@daily_attachment)
    assert_response :success
  end

  test "should get edit" do
    get edit_daily_attachment_url(@daily_attachment)
    assert_response :success
  end

  test "should update daily_attachment" do
    patch daily_attachment_url(@daily_attachment), params: { daily_attachment: { avatar: @daily_attachment.avatar, daily_id: @daily_attachment.daily_id } }
    assert_redirected_to daily_attachment_url(@daily_attachment)
  end

  test "should destroy daily_attachment" do
    assert_difference('DailyAttachment.count', -1) do
      delete daily_attachment_url(@daily_attachment)
    end

    assert_redirected_to daily_attachments_url
  end
end
